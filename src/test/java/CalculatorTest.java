import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @ParameterizedTest
    @CsvSource(
            value = {
                    "1,2,3",
                    "10000000,10000000,20000000",
                    "1500,500,2000"
            },
            delimiter = ','
    )
    public void addEquals(int a, int b, int expected) {
        Calculator.add(a,b);
        assertEquals(calculatePrintFormatter(a, b, expected, '+'), outputStreamCaptor.toString().trim(), "Result Not Same");
    }

    @Test
    public void addDoesNotThrownTest() {
        assertDoesNotThrow(() -> {
            Calculator.add(2_000_000_000, 2_000_000_000);
        });
    }

    @ParameterizedTest
    @CsvSource(
            value = {
                    "4,2,2",
                    "5,2,3",
                    "2,3,-1"
            },
            delimiter = ','
    )
    public void subtractTrue(int a, int b, int expected) {
        Calculator.subtract(a, b);
        assertTrue(calculatePrintFormatter(a, b, expected, '-').equals(outputStreamCaptor.toString().trim()));
    }

    @ParameterizedTest
    @CsvSource(
            value = {
                    "4,100,2",
                    "5,5,3",
                    "2,100,-1"
            },
            delimiter = ','
    )
    public void subtractFalse(int a, int b, int expected) {
        Calculator.subtract(a, b);
        assertFalse(calculatePrintFormatter(a, b, expected, '-').equals(outputStreamCaptor.toString().trim()));
    }

    @ParameterizedTest
    @CsvSource(
            value = {
                    "4,2,8",
                    "-1,-1,1",
                    "1000,0,0"
            },
            delimiter = ','
    )
    public void multiplyEquals(int a, int b, int expected) {
        Calculator.multiply(a, b);
        assertEquals(calculatePrintFormatter(a, b, expected, '*'), outputStreamCaptor.toString().trim());
    }

    @ParameterizedTest
    @CsvSource(
            value = {
                    "4,2,7",
                    "-1,-1,-1",
                    "1001,0,1"
            },
            delimiter = ','
    )
    public void multiplyFalse(int a, int b, int expected) {
        Calculator.multiply(a, b);
        assertFalse(calculatePrintFormatter(a, b, expected, '*').equals(outputStreamCaptor.toString().trim()));
    }

    @ParameterizedTest
    @CsvSource(
            value = {
                    "4,2,2",
                    "5,2,2",
                    "2,3,0"
            },
            delimiter = ','
    )
    public void divide(int a, int b, int expected) {
        Calculator.divide(a, b);
        assertEquals(calculatePrintFormatter(a, b, expected, '/'), outputStreamCaptor.toString().trim());
    }

    @Test
    public void divideIsThrownTest() {
        assertThrows(ArithmeticException.class, () -> {
            Calculator.divide(1,0);
        });
    }

    private String calculatePrintFormatter(int a, int b, long result, char operation) {
        return String.format("%d %c %d = %d", a, operation, b, result);
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }
}