# rewind-intermediate

### Nexsoft Fun Coding Bootcamp Batch 7 Day 15

## Author
### Bayu Seno Ariefyanto

## Preview Image Running Test
![Main Menu](running-test.png)

## Files Directory
```
📦RewindIntermediate
 ┣ 📂.idea
 ┣ 📂.settings
 ┣ 📂src
 ┃ ┣ 📂main
 ┃ ┃ ┣ 📂java
 ┃ ┃ ┃ ┗ 📜Calculator.java
 ┃ ┃ ┗ 📂resources
 ┃ ┗ 📂test
 ┃ ┃ ┗ 📂java
 ┃ ┃ ┃ ┗ 📜CalculatorTest.java
 ┣ 📂target
 ┣ 📜pom.xml
 ┗ 📜README.md
```